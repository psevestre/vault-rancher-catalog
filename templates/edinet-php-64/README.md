# EDINET PHP Batch Template - 64 Bits

Template para executar uma rotina batch da EDINET, utilizando PHP 5.3.2
em ambiente de 64 bits.

## Configurações necessárias no ambiente

1. Vault

Criar um "secret" contendo as propriedades "id_rsa" e "id_rsa.pub". Estas entradas
devem conter o par de chaves para acesso o SVN. A chave pública deve ser adicionada
manualmente no servidor SVN, no home do usuário utilizado para fazer o checkout.

Por padrão, este usuário é o "edinet".


